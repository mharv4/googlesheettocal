const fs = require('fs');
const readline = require('readline');
const { google } = require('googleapis');

const SCOPES = ['https://www.googleapis.com/auth/calendar',
    'https://www.googleapis.com/auth/spreadsheets'];

const TOKEN_PATH = 'token.json';

const SheetId = "INSERT TOKEN HERE"


fs.readFile('credentials.json', (err, content) => {
    if (err) return console.log('Error loading client secret file:', err);
    //Call the create event function
    authorize(JSON.parse(content), getSheet);
    //  authorize(JSON.parse(content), createEvent);
});


//Auth Garbage
function authorize(credentials, callback) {
    const { client_secret, client_id, redirect_uris } = credentials.installed;
    const oAuth2Client = new google.auth.OAuth2(
        client_id, client_secret, redirect_uris[0]);

    // Check if we have previously stored a token.
    fs.readFile(TOKEN_PATH, (err, token) => {
        if (err) return getAccessToken(oAuth2Client, callback);
        oAuth2Client.setCredentials(JSON.parse(token));
        callback(oAuth2Client);
    });
}

function getAccessToken(oAuth2Client, callback) {
    const authUrl = oAuth2Client.generateAuthUrl({
        access_type: 'offline',
        scope: SCOPES,
    });
    console.log('Authorize this app by visiting this url:', authUrl);
    const rl = readline.createInterface({
        input: process.stdin,
        output: process.stdout,
    });
    rl.question('Enter the code from that page here: ', (code) => {
        rl.close();
        oAuth2Client.getToken(code, (err, token) => {
            if (err) return console.error('Error retrieving access token', err);
            oAuth2Client.setCredentials(token);
            // Store the token to disk for later program executions
            fs.writeFile(TOKEN_PATH, JSON.stringify(token), (err) => {
                if (err) return console.error(err);
                console.log('Token stored to', TOKEN_PATH);
            });
            callback(oAuth2Client);
        });
    });
}
//END Auth Garbage


function getSheet(auth) {
    const sheets = google.sheets('v4');
    var request = {
        spreadsheetId: SheetId,
        includeGridData: true,
        ranges: [],  // TODO: Update placeholder value.
        auth: auth,
    };

    sheets.spreadsheets.get(request, function (err, response) {
        if (err) {
            console.error(err);
            return;
        }
        let rowData = response.data.sheets[0].data[0].rowData


        //Go through each row, and make 
        eventList = [];
        rowData.forEach(row => {
            let date = row.values[0].userEnteredValue.stringValue
            let title = row.values[1].userEnteredValue.stringValue
            let body = row.values[2].userEnteredValue.stringValue

            eventList.push({ data: date, title: title, body: body })
        });
//Parse the date here, then call the 
        console.log(JSON.stringify(eventList))
createEvent(auth, "TestEvent123")

    });
}


function createEvent(auth, title) {
    var event = {
        'summary': title,
        'description': 'Testing an event creation',
        'start': {
            'dateTime': '2019-08-15T17:00:00-07:00',
            'timeZone': 'Australia/Brisbane'
        },
        'end': {
            'dateTime': '2019-08-15T17:00:00-14:00',
            'timeZone': 'Australia/Brisbane'
        }
    };

    const calendar = google.calendar({ version: 'v3', auth });
    calendar.events.insert({
        'calendarId': 'primary',
        'resource': event
    }, (err, res) => {
        if (err) {
            return console.log('The API returned an error: ' + err);
        }
        console.log("Success")
        //console.log(JSON.stringify(res))
    });


}